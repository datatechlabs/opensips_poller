#!/usr/bin/env python

#
# aswitch setup script
#

import os
from distutils.core import setup

def listdir(path, prefix, add_path=True):
        """List all files within a directory recursively.

	path -- local directory where the files are taken from
	prefix -- add this prefix to destination directory
	add_path -- whether to add path to destination dir (after prefix)
	
	Returns a list of file names like this:
		[(dir1, file_list1),
		 (dir2, file_list2),
		 (dir3, file_list3),
		 ...
		]
        """
        files = []
	dirs = []

	for name in os.listdir(path):
		# exclude hidden files
		if name.startswith("."):
			continue
		name = "%s/%s" % (path, name)
		
		# go into directories recursively
		if os.path.isdir(name):
			dirs.append(name)
			continue
		files.append(name)
		
	if add_path:
		result = [(prefix + "/" + path, files)]
	else:
		result = [(prefix, files)]

	for dir in dirs:
		result += listdir(dir, prefix)

	return result
        
# prepare setup data
conf_dir = "etc"
log_dir = "var/log"

data_files = []
data_files += listdir("etc", conf_dir, False)

# some (initially) empty directories
data_files += [(log_dir, [])]

# install aswitch
dist = setup(
	name = "opensips_poller",
	version = "1.0-STABLE",
	description = "OpenSIPS poller",
	author = "DataTechLabs",
	author_email = "info@datatechlabs.com",
	url = "http://www.datatechlabs.com",
	packages = ["opensips_poller"],
	scripts = ["bin/opensips_poller"],
	data_files = data_files
)
