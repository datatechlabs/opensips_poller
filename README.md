# OpenSIPS Poller #

## Installation ##
```python setup.py install```

## Usage ##

 To check version and see usage:


```
#!sh

opensips_poller -h
```


You should see like this:


```
#!sh

OpenSIPS poller 1.0-STABLE

        -h                      this help message
        -c config_file          configuration file path
        -D                      debug mode
        -g                      Get the current status of the poller and quit
        -s 'YYYY-MM-DD hh:mm:ss'                        Set the poller to the specified time and quit
```


To check current state:


```
#!sh

opensips_poller -g
```


To set the required start time:


```
#!sh

opensips_poller -s 'YYYY-MM-DD hh:mm:ss'
```


Always double check if it was set properly.

To start processing:


```
#!sh

opensips_poller
```

